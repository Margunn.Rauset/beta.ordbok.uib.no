# beta.ordbok.uib.no

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Universell utforming
Nettsiden kan brukes med en skjermleser. Man kan navigere gjennom siden med Tab. Pileknapper må brukes for å navigere mellom
radioknapper ved valget av målformen og ved navigeringen opp/ned i dropdown-menyen i søkefeltet. Det finnes en ekspress-navigasjon 
til søkefeltet, men snarveien kan være forskjellig avhengig av nettleseren og operativsystemet. "Nøkkelbokstav" er "s": f.eks, 
på Linux er det alt+s i Chrome og shift+alt+s i Firefox. En full oversikt over flere nettlesere og OS finnes i en tabell 
her https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/accesskey

### Workflow
Create a merge request and merge your branch into master if and only if the issue is tagged as ready for prod.
Before submitting the merge request, you must pull any changes from the master branch, resolve merge conflicts and test the result on the dev server.
Deployment to dev server is now accessible on all branches. You can re-run deploy-dev without build-dev to deploy a particular branch to dev (within
the expiry date of 1 week).