module.exports = {
  runtimeCompiler: true,

  transpileDependencies: [
    'vuetify'
  ],

  devServer: {
    https: false
  },

  pluginOptions: {
    i18n: {
      fallbackLocale: 'eng',
      localeDir: 'locales',
      enableInSFC: true,
      includeLocales: false,
      enableBridge: true
    }
  }
}
