import Vue from 'vue'
import Root from './Root.vue'
import App from './App.vue'
import About from './components/About.vue'
import DictionaryView from './components/DictionaryView.vue'
import VueRouter from 'vue-router'
import { VuePlausible } from 'vue-plausible'
import vuetify from './plugins/vuetify'
import Vuex from 'vuex'
import i18n from './i18n'


Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VuePlausible, {
  domain: 'beta.ordbok.uib.no'
})

Vue.$plausible.enableAutoPageviews()

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      component: App,
      children: [
        {
          path: 'om',
          name: 'about',
          component: About
        },
        {
          path: '',
          component: DictionaryView,
          children: [
            {
              path: ':lang(bm|nn|bm,nn)',
              children: [
                {
                  path: 'search',
                  name: 'query',
                  params: {q: '', scope: 'w'}
                },
                {
                  name: 'word',
                  path: 'w/:query'
                },
                {
                  name: 'lookup',
                  path: ':id(\\d+)/:lemma?'
                },
                {
                  name: 'search',
                  path: 'search/:query'
                }
              ]
            },
            {
              path: 'bob/*',
              redirect: to => {
                return "bm/" + to.params.pathMatch
              }
            },
            {
              path: 'nob/*',
              redirect: to => {
                return "nn/" + to.params.pathMatch
              }
            },
            {
              path: 'bob,nob/*',
              redirect: to => {
                return "bm,nn/" + to.params.pathMatch
              }
            }
          ]
        }
      ]
    }
  ]
})

// All interaction with local storage is encapsulated in vuex
const store = new Vuex.Store({
  strict: true,
  state: {
    showSearchToolbar: null,
    showInflectionNo: null,
    currentLocale: null,
    collapseArticles: null,
    defaultDict: null,
    menuOpen: false,
    noMouse: null,
    searchRoute: null,
  },
  mutations: {
    initStore(state) {
      state.showSearchToolbar = localStorage.getItem('showSearchToolbar') || false
      state.showInflectionNo = localStorage.getItem('showInflectionNo') || false
      state.currentLocale = localStorage.getItem('currentLocale')
      state.defaultDict = localStorage.getItem('defaultDict') || 'bm,nn'
      state.collapseArticles = localStorage.getItem('collapseArticles') || 'auto'
      state.noMouse = window.matchMedia('(hover: none)').matches

      
      if (!state.currentLocale) {
        let locale = navigator.language.split("-")[0]
        if (locale == "nn") state.currentLocale = "nno"
        else if (locale == "nb") state.currentLocale = "nob"
      }

      if (state.currentLocale) {
        i18n.locale = state.currentLocale
      }

    },
    setLocale(state, payload) {
      Vue.$plausible.trackEvent("set locale", {props: {from: state.currentLocale, to: payload.locale}})
      state.currentLocale = payload.value
      i18n.locale = payload.value
      localStorage.setItem("currentLocale", payload.value);
    },
    setCollapse(state, value) {
      Vue.$plausible.trackEvent("set collapse", {props: {from: state.collapseArticles, to: value}})
      localStorage.setItem("collapseArticles", value)
      state.collapseArticles = value
    },
    toggleInflectionNo(state) {
      Vue.$plausible.trackEvent("toggle inflection number", {props: {from: state.showInflectionNo, to: !state.showInflectionNo}})
      state.showInflectionNo = !state.showInflectionNo
      localStorage.setItem('showInflectionNo', state.showInflectionNo);
    },
    toggleMenu(state) {
      state.menuOpen = !state.menuOpen
    },
    setDefaultDict(state, value) {
      Vue.$plausible.trackEvent("set default dict", {props: {from: state.defaultDict, to: value}})
      localStorage.setItem("defaultDict", value)
      state.defaultDict = value
    },
    resetStore() {
      Vue.$plausible.trackEvent("reset store")
      localStorage.removeItem("showSearchToolbar")
      localStorage.removeItem("showInflectionNo")
      localStorage.removeItem("currentLocale")
      localStorage.removeItem("collapseArticles")
      localStorage.removeItem("defaultDict")
      this.commit("initStore")
    },
    setSearchRoute(state, path) {
      state.searchRoute = path
    }
  }
})

new Vue({
  router,
  vuetify,
  store,

  beforeCreate() {
		this.$store.commit('initStore');
	},

  i18n,
  render: h => h(Root )
}).$mount('#app')
