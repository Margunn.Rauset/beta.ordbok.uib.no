const mapping = {
  "bm": {
    "NOUN": "substantiv",
    "VERB": "verb",
    "ADJ": "adjektiv",
    "ADP": "preposisjon",
    "PFX": "prefiks",
    "ADV": "adverb",
    "DET": "determinativ",
    "PROPN": "egennavn",
    "ABBR": "forkortelse",
    "INTJ": "interjeksjon",
    "SYM": "symbol",
    "PRON": "pronomen",
    "CCONJ": "konjunksjon",
    "SCONJ": "subjunksjon",
    "INFM": "infinitivsmerke",
    "COMPPFX": "i sammensetting",
    "Masc": "hankjønn",
    "Fem": "hunkjønn",
    "Neuter": "intetkjønn",
    "Uninfl": "ubøyelig"
  },
  "nn": {
    "NOUN": "substantiv",
    "VERB": "verb",
    "ADJ": "adjektiv",
    "ADP": "preposisjon",
    "PFX": "prefiks",
    "ADV": "adverb",
    "DET": "determinativ",
    "PROPN": "eigennamn",
    "ABBR": "forkorting",
    "INTJ": "interjeksjon",
    "SYM": "symbol",
    "PRON": "pronomen",
    "CCONJ": "konjunksjon",
    "SCONJ": "subjunksjon",
    "INFM": "infinitivsmerke",
    "COMPPFX": "i samansetjing",
    "Masc": "hankjønn",
    "Fem": "hokjønn",
    "Neuter": "inkjekjønn",
    "Uninfl": "ubøyeleg"
  }
}

var group_list = function(grps, dictionary) {
  var grp_collection = new Set()
  var noun_grp_collection = new Set()
  grps.forEach(function(lemma){
    lemma.paradigm_info.forEach(function(grp){
      if (grp.tags){
        if(grp.tags[0] == 'NOUN') {
          noun_grp_collection.add(grp.tags[1])
        }else{
          grp_collection.add(mapping[dictionary][grp.tags[0]])
        }
      }
    })
  })
  if (noun_grp_collection.size) {
    let noun_grp_text = 'substantiv  '
    if (noun_grp_collection.size == 3) {
      noun_grp_text += mapping[dictionary]['Masc'] + ', ' + mapping[dictionary]['Fem'] + ' eller ' + mapping[dictionary]['Neuter']
    } else {
      noun_grp_text += Array.from(noun_grp_collection).map(code => mapping[dictionary][code]).sort().join(' eller ')
    }
    grp_collection.add(noun_grp_text)
  }
  return Array.from(grp_collection).join(' - ')
}

var fraction = function(numerator, denominator) {
  var superscript = {
  '0': '⁰',
  '1': '¹',
  '2': '²',
  '3': '³',
  '4': '⁴',
  '5': '⁵',
  '6': '⁶',
  '7': '⁷',
  '8': '⁸',
  '9': '⁹',
  '+': '⁺',
  '-': '⁻',
  '=': '⁼',
  '(': '⁽',
  ')': '⁾',
  'a': 'ᵃ',
  'b': 'ᵇ',
  'c': 'ᶜ',
  'd': 'ᵈ',
  'e': 'ᵉ',
  'f': 'ᶠ',
  'g': 'ᵍ',
  'h': 'ʰ',
  'i': 'ⁱ',
  'j': 'ʲ',
  'k': 'ᵏ',
  'l': 'ˡ',
  'm': 'ᵐ',
  'n': 'ⁿ',
  'o': 'ᵒ',
  'p': 'ᵖ',
  'r': 'ʳ',
  's': 'ˢ',
  't': 'ᵗ',
  'u': 'ᵘ',
  'v': 'ᵛ',
  'w': 'ʷ',
  'x': 'ˣ',
  'y': 'ʸ',
  'z': 'ᶻ',
  ' ': ' '
  }

  var subscript = {
  '0': '₀',
  '1': '₁',
  '2': '₂',
  '3': '₃',
  '4': '₄',
  '5': '₅',
  '6': '₆',
  '7': '₇',
  '8': '₈',
  '9': '₉',
  '+': '₊',
  '-': '₋',
  '=': '₌',
  '(': '₍',
  ')': '₎',
  'a': 'ₐ',
  'e': 'ₑ',
  'h': 'ₕ',
  'i': 'ᵢ',
  'j': 'ⱼ',
  'k': 'ₖ',
  'l': 'ₗ',
  'm': 'ₘ',
  'n': 'ₙ',
  'o': 'ₒ',
  'p': 'ₚ',
  'r': 'ᵣ',
  's': 'ₛ',
  't': 'ₜ',
  'u': 'ᵤ',
  'v': 'ᵥ',
  'x': 'ₓ',
  ' ': ' '
  };

  var fractions = {
  '1/2': '½',
  '1/3': '⅓',
  '2/3': '⅔',
  '1/4': '¼',
  '3/4': '¾',
  '1/5': '⅕',
  '2/5': '⅖',
  '3/5': '⅗',
  '4/5': '⅘',
  '1/6': '⅙',
  '5/6': '⅚',
  '1/7': '⅐',
  '1/8': '⅛',
  '3/8': '⅜',
  '5/8': '⅝',
  '7/8': '⅞',
  '1/9': '⅑',
  '1/10': '⅒'
  };

  let other_fraction = null

  let num_sup = numerator.toString().split('').map(x => superscript[x] || '_').join('')
  let den_sub = denominator.toString().split('').map(x => subscript[x] || '_').join('')

  if (! num_sup.includes('_') && ! den_sub.includes('_')) {
    other_fraction = num_sup + '⁄' +  den_sub
  }

  return fractions[numerator + '/' + denominator] && {type: 'plain', html: fractions[numerator + '/' + denominator] }
      || other_fraction && {type: 'plain', html: other_fraction }
      || {type: 'fraction', html: '', num: numerator, denom: denominator}

}

const roman_hgno = function(lemma) {
  let hgint = parseInt(lemma.hgno)
  if (hgint > 0) {
    return ["I","II","III","IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI"][hgint-1]
  }
  return ""
}

export default { group_list, fraction, roman_hgno, mapping }
